#include <MIDI.h>

#include <Midi.h>
#define LED 13 

MIDI_CREATE_DEFAULT_INSTANCE(); //creates a midi object called "midi"

void setup() {
  
  // put your setup code here, to run once:
  //LED is currently at pin 13, but can just change whatever pin we want from the chip
  pinMode(LED, OUTPUT); //sets LED pin to be the output, this will change but for debugging)
  
  MIDI.begin(MIDI_CHANNEL_OMNI); //enable communication on the serial port
  MIDI.setHandleNoteOn(MyHandleNoteOn); //midi library command when an event comes in, MyHandleNoteOn is the function that deals
  //with it
  MIDI.setHandleNoteOff(MyHandleNoteOff); //same for note off message
}

void loop() {
  // put your main code here, to run repeatedly:
  MIDI.read(); //continously checks the arduino serial input, should not wait too long
}
void MyHandleNoteOn(byte channel, byte pitch, byte velocity)
{
  //write case statements here
  
  digitalWrite(LED, HIGH);//Turns LED or pin 13 high on
}

void MyHandleNoteOff(byte channel, byte pitch, byte velocity)
{
  digitalWrite(LED, LOW);
}
