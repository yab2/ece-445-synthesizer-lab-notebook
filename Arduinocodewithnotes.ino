#include <MIDI.h>

#include <Midi.h>
#define LED 13 
#define OUTPIN 4 //could be 4-13


//2.5V maps to 440Hz
//1.3V maps to 220 Hz

//3.7 maps to 880Hz

MIDI_CREATE_DEFAULT_INSTANCE(); //creates a midi object called "midi"

void setup() {
  // put your setup code here, to run once:
  //LED is currently at pin 13, but can just change whatever pin we want from the chip
  pinMode(LED, OUTPUT); //sets LED pin to be the output, this will change but for debugging)
  
  MIDI.begin(MIDI_CHANNEL_OMNI); //enable communication on the serial port
  MIDI.setHandleNoteOn(MyHandleNoteOn); //midi library command when an event comes in, MyHandleNoteOn is the function that deals
  //with it
  MIDI.setHandleNoteOff(MyHandleNoteOff); //same for note off message
}

void loop() {
  // put your main code here, to run repeatedly:
  MIDI.read(); //continously checks the arduino serial input, should not wait too long
}
void MyHandleNoteOn(byte channel, byte pitch, byte velocity)
{
  //write case statements here
  switch (pitch)
    {
        case 57:
            analogWrite(OUTPIN, 66.56 ); //1.3V, 220HZ
            break;
        case 58:
            analogWrite(OUTPIN, 71.68); //1.4 V, 233.082HZ
            break;
        case 59:
            analogWrite(OUTPIN, 76.8 ); //1.5V, 246.942 HZ
            break;
        case 60:
            analogWrite(OUTPIN, 81.92); //1.6V, 261.62 6Hz
            break;
        case 61:
            analogWrite(OUTPIN, 87.04 ); //1.7 V, 277.183 Hz
            break;
        case 62:
            analogWrite(OUTPIN, 92.16 ); //1.8V, 293.66 5Hz
            break;
        case 63:
            analogWrite(OUTPIN, 97.28); //1.9V, 311.127Hz
            break;
        case 64:
            analogWrite(OUTPIN, 102.4); //2.0V, 329.628V
            break;
        case 65:
            analogWrite(OUTPIN, 107.52); //2.1V, 349.228Hz
            break;
        case 66:
            analogWrite(OUTPIN, 112.64); //2.2V, 369.994V
            break;
        case 67:
            analogWrite(OUTPIN, 117.76); //2.3V, 391.995V
            break;
        case 68:
            analogWrite(OUTPIN, 122.88); //2.4V, 415.305Hz
            break;
        case 69:
            analogWrite(OUTPIN, 128); //440HZ, 2.5V, 128 PWM output
            break;
        case 70:
            analogWrite(OUTPIN, 133.12); //2.6V
            break;

        case 71:
            analogWrite(OUTPIN, 138.24 ); //2.7V
            break;
        case 72:
            analogWrite(OUTPIN, 143.36 ); //2.8V
            break;

        case 73:
            analogWrite(OUTPIN, 148.48); //2.9V
            break;

        case 74:
            analogWrite(OUTPIN, 153.6 ); //3.0V
            break;

        case 75:
            analogWrite(OUTPIN, 158.72); //3.1V
            break;
            
        case 76:
            analogWrite(OUTPIN, 163.84); //3.2V
            break;

        case 77:
            analogWrite(OUTPIN, 168.96); //3.3V
            break;

        case 78:
            analogWrite(OUTPIN, 174.08); //3.4V
            break;

        case 79:
            analogWrite(OUTPIN, 179.2); //3.5V
            break;

        case 80:
            analogWrite(OUTPIN, 184.32); //3.6V
            break;
              
        default:
            analogWrite(OUTPIN, LOW);
            break; 
    }
}

void MyHandleNoteOff(byte channel, byte pitch, byte velocity)
{
  digitalWrite(LED, LOW);
}
